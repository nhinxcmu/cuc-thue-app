import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/pages/CamOnPage/CamOn.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/DanhSachTinNhanPage.dart';
import 'package:cuc_thue/pages/DocTinNhanPage/DocTinNhanPage.dart';
import 'package:cuc_thue/pages/GuiThongTinPage/GuiThongTinPage.dart';
import 'package:cuc_thue/pages/PushMessagePage/PushMessagePage.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'pages/LoginPage/LoginPage.dart';
import 'pages/HomePage/HomePage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // initLocalNotification();
  runApp(MyApp());
}

final FirebaseAuth _auth = FirebaseAuth.instance;
troLaiTrangLogin(BuildContext context) {
  Navigator.popUntil(context, (route) => !Navigator.canPop(context));
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => LoginPage()));
}







class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Widget initPage = LoadingPage();
  TextTheme newTextTheme;
  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([
      SystemUiOverlay.bottom,
    ]);
    newTextTheme = Theme.of(context).textTheme.apply(
        bodyColor: secondaryColor,
        displayColor: secondaryColor,
        decorationColor: primaryColor);

    _auth.currentUser().then((user) {
      if (user == null) {
        setState(() {
          initPage = LoginPage();
        });
      } else {
        setState(() {
          initPage = HomePage();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          textTheme: newTextTheme,
          primaryColor: primaryColor,
          accentColor: primaryColor),
      //home: DanhSachTinNhanPage()
      home: initPage,
      //home: SafeArea(child: DanhSachTinNhanPage())
      //home: DocTinNhanPage(mail: null),
      //home: SafeArea(child: CamOnPage()),
      //home:GuiThongTinPage()
      //home: PushMessagePage(),
    );
  }
}

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: CircularProgressIndicator(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
