import 'dart:convert';
import 'dart:io';

import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Mail.dart';
import 'package:flutter/widgets.dart';
import 'package:http_parser/http_parser.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class MobileService {
  SharedPreferences prefs;
  Future<int> layUserId() async {
    prefs = await SharedPreferences.getInstance();
    return prefs.getInt("userId");
  }

  void luuTinNhan(
      BuildContext context,
      List<File> files,
      String subject,
      String content,
      int rid,
      int pmid,
      LocationData locationData,
      Function callBack) async {
    var userId = await layUserId();
    var uri = Uri.parse(host + '/mobile/luu-tin-nhan-v2');
    var request = http.MultipartRequest('POST', uri);
    String authorization =
        "Basic " + base64Encode(utf8.encode('defuser:Cmu#2018'));
    request.headers["Authorization"] = authorization;
    for (var file in files) {
      request.files.add(http.MultipartFile.fromBytes(
          "files", await file.readAsBytes(),
          contentType: MediaType.parse("image/png"),
          filename: file.path.substring(file.path.lastIndexOf("/") + 1)));
    }
    request.fields["subject"] = subject;
    request.fields["content"] = content;
    request.fields["sid"] = userId.toString();
    request.fields["pmid"] = pmid!=null?pmid.toString():"";
    request.fields["rid"] = rid.toString();
    request.fields["lat"] = locationData.latitude.toString();
    request.fields["lon"] = locationData.longitude.toString();

    var res = await request.send();
    if (callBack != null) callBack(res);
  }

  void docTinNhan(int mid, Function(List<Mail>) callBack) async {
    prefs = await SharedPreferences.getInstance();

    var map = new Map<String, dynamic>();
    map['uid'] = prefs.getInt("userId").toString();
    map['mid'] = mid.toString();

    var headers = new Map<String, String>();
    String authorization =
        "Basic " + base64Encode(utf8.encode('defuser:Cmu#2018'));
    headers["Authorization"] = authorization;

    http
        .post(host + '/mobile/doc-tin-nhan', body: map, headers: headers)
        .then((res) {
      List body = json.decode(res.body);
      var _mails = body.map((map) => Mail.fromMap(map)).toList();
      callBack(_mails);
    }).catchError((error) {
      print(error);
    });
  }
}
