import 'dart:io';
import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/pages/CamOnPage/CamOn.dart';
import 'package:cuc_thue/services/MobileService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormGuiThongTin extends StatefulWidget {
  const FormGuiThongTin({
    Key key,
  }) : super(key: key);

  @override
  _FormGuiThongTinState createState() => _FormGuiThongTinState();
}

class _FormGuiThongTinState extends State<FormGuiThongTin> {
  MobileService mobileService = MobileService();
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  List<File> files = [];
  String subject = "";
  String content = "";
  int pmid;
  int sid = 15;
  int rid = 1;

  FocusNode focusNode;
  bool disabled = false;
  Future<void> layViTri() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.GRANTED) {
        return;
      }
      if (_permissionGranted != PermissionStatus.DENIED) {
        Navigator.pop(context);
      }
    }

    _locationData = await location.getLocation();
  }

  Future<int> layUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("userId");
  }

  void guiTinNhanTap(BuildContext context) async {
    setState(() {
      disabled = true;
    });
    mobileService.luuTinNhan(
        context, files, subject, content, rid, null, _locationData, (res) {
      setState(() {
        disabled = false;
      });
      if (res.statusCode == 200)
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => CamOnPage()));
      else
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Có lỗi khi gửi thông tin, vui lòng thử lại! (" +
              res.statusCode.toString() +
              ")"),
        ));
    });
  }

  Future getImageFromCamera() async {
    // FocusScope.of(context).unfocus(focusPrevious: true);
    var file = await ImagePicker.pickImage(source: ImageSource.camera);
    if (file != null) {
      setState(() {
        files.add(file);
      });
    }
  }

  Future getImageFromFile() async {
    // FocusScope.of(context).unfocus(focusPrevious: true);
    var file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file != null) {
      setState(() {
        files.add(file);
      });
      print(content);
    }
  }

  String getTieuDeMacDinh() {
    return "Tin nhắn ngày " + formatter.format(DateTime.now());
  }

  @override
  void initState() {
    super.initState();
    layViTri();
    focusNode = FocusNode();
    setState(() {
      subject = getTieuDeMacDinh();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: boxShadow,
              borderRadius: BorderRadius.circular(8.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: TextFormField(
                  enabled: !disabled,
                  // focusNode: focusNode,
                  cursorColor: Color.fromRGBO(0, 0, 0, 0.8),
                  textInputAction: TextInputAction.next,
                  initialValue: getTieuDeMacDinh(),
                  decoration: InputDecoration(
                    isDense: true,
                    prefixIcon: Icon(Icons.title),
                    border: OutlineInputBorder(),
                    hintText: "Nhà hàng ABC không xuất hóa đơn ngày...",
                    hasFloatingPlaceholder: true,
                    alignLabelWithHint: true,
                    labelText: "Chủ đề",
                  ),
                  onChanged: (value) {
                    setState(() {
                      subject = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Không được để trống!';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: TextFormField(
                  enabled: !disabled,
                  // focusNode: focusNode,
                  cursorColor: Color.fromRGBO(0, 0, 0, 0.8),
                  textInputAction: TextInputAction.next,
                  minLines: 3,
                  maxLines: 10,
                  decoration: InputDecoration(
                    isDense: true,
                    prefixIcon: Icon(Icons.text_fields),
                    border: OutlineInputBorder(),
                    hintText:
                        "Hôm thứ 7 rồi, sau một tuần làm việc căng thẳng, anh Dư có dẫn tôi....",
                    hasFloatingPlaceholder: true,
                    labelText: "Nội dung",
                  ),
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      content = value;
                    });
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Không được để trống!';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(0, 0, 0, 0.4)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  height: 140,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          color: Color.fromRGBO(0, 0, 0, 0.02),
                          child: files.length > 0
                              ? ListView.builder(
                                  itemCount: files.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return Stack(
                                        alignment: Alignment.topRight,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: 8,
                                                bottom: 8,
                                                right: 8,
                                                left: index == 0 ? 8 : 0),
                                            child: Container(
                                              height: 140,
                                              width: 100,
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                image: FileImage(files[index]),
                                                fit: BoxFit.cover,
                                              )),
                                              child: Material(
                                                color: Colors.transparent,
                                                child: InkWell(
                                                  splashColor: primaryColor
                                                      .withOpacity(0.3),
                                                  onTap: () {},
                                                ),
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                              icon: Icon(
                                                Icons.remove_circle_outline,
                                                color: Colors.white,
                                              ),
                                              onPressed: () {
                                                files.removeAt(index);
                                                setState(() {
                                                  files = files;
                                                });
                                              })
                                        ]);
                                  },
                                )
                              : Material(
                                  child: InkWell(
                                    splashColor: primaryColor.withOpacity(0.3),
                                    onTap: getImageFromCamera,
                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 140,
                                      child: Text(
                                        "Chưa có hình ảnh",
                                        style: TextStyle(
                                            color: Color.fromRGBO(0, 0, 0, 0.5),
                                            fontStyle: FontStyle.italic),
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(),
                        width: 70,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Expanded(
                                  child: Material(
                                color: primaryColor,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(4)),
                                child: InkWell(
                                  onTap: disabled ? null : getImageFromCamera,
                                  child: Icon(
                                    Icons.add_a_photo,
                                    color: Colors.white,
                                  ),
                                ),
                              )),
                              Expanded(
                                  child: Material(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(4)),
                                child: InkWell(
                                  onTap: disabled ? null : getImageFromFile,
                                  child: Icon(Icons.add_photo_alternate,
                                      color: primaryColor),
                                ),
                              )),
                            ]),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: RaisedButton(
                  disabledColor: Colors.grey,
                  // focusNode: focusNode,
                  splashColor: Color.fromRGBO(255, 255, 255, 0.5),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  padding: EdgeInsets.only(
                      top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
                  color: primaryColor,
                  onPressed: disabled ? null : () => guiTinNhanTap(context),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Gửi thông tin',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            letterSpacing: 2),
                      ),
                      disabled
                          ? Padding(
                              padding: EdgeInsets.only(left: 15),
                              child: Container(
                                width: 15,
                                height: 15,
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                  strokeWidth: 3,
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
