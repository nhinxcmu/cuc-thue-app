import 'package:cuc_thue/pages/GuiThongTinPage/FormGuiThongTin.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:flutter/material.dart';

class GuiThongTinPage extends StatefulWidget {
  @override
  _GuiThongTinPageState createState() => _GuiThongTinPageState();
}

class _GuiThongTinPageState extends State<GuiThongTinPage>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
  }

  Future<bool> _onWillPop() async {
    await _animationController.reverse();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    double vh = MediaQuery.of(context).size.height / 100;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Stack(children: [
          Column(
            children: <Widget>[
              Expanded(
                child: Hero(
                  tag: "background",
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/background.png"),
                            fit: BoxFit.fitWidth,
                            alignment: Alignment.topCenter)),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              child: FadeUpListView(
                animationController: _animationController,
                offsetY: -60,
                whenToDoNextAnimation: 0.3,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 12 * vh),
                    child: Text(
                      "Tin báo",
                      style: TextStyle(
                          fontSize: 50,
                          color: Color.fromRGBO(0, 0, 0, 0.8),
                          letterSpacing: 2),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 12 * vh),
                    child: Text(
                      "Thông tin chi tiết sẽ giúp chúng tôi xác định vụ việc và xử lý nhanh chóng hơn",
                      style: TextStyle(
                        color: Color.fromRGBO(0, 0, 0, 0.8),
                        letterSpacing: 2,
                        fontStyle: FontStyle.italic,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  FormGuiThongTin(),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
