import 'package:cuc_thue/pages/DanhSachTinNhanPage/DanhSachTinNhanPage.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../../../main.dart';

class PanelUser extends StatefulWidget {
  Function onMailBoxTap;
  PanelUser({this.onMailBoxTap});
  @override
  _PanelUserState createState() => _PanelUserState();
}

class _PanelUserState extends State<PanelUser> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser currentUser;
  @override
  void initState() {
    super.initState();
    _auth.onAuthStateChanged.listen((user) {
      if (user == null) troLaiTrangLogin(context);
    });
    _auth.currentUser().then((user) {
      setState(() {
        currentUser = user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'panelUser',
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 60,
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/user-panel-background.png"),
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.centerRight),
              borderRadius: BorderRadius.all(Radius.circular(50)),
              color: Colors.white,
              boxShadow: boxShadow),
          child: Row(
            children: <Widget>[
              CircleAvatar(
                backgroundImage:
                    currentUser != null && currentUser.photoUrl != null
                        ? NetworkImage(currentUser.photoUrl)
                        : AssetImage("assets/avatar.png"),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                          currentUser != null
                              ? (currentUser.displayName != "" &&
                                      currentUser.displayName != null
                                  ? currentUser.displayName
                                  : currentUser.phoneNumber)
                              : "",
                          style: Theme.of(context).textTheme.body1),
                      widget.onMailBoxTap != null
                          ? Material(
                              color: Colors.transparent,
                              child: InkWell(
                                child: Container(
                                    height: 25,
                                    width: 25,
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                    ),
                                    child: Icon(Icons.inbox)),
                                onTap: widget.onMailBoxTap,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50)),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  child: Container(
                    height: 55,
                    width: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                    ),
                    child: Icon(
                      Icons.input,
                      color: Colors.white,
                    ),
                  ),
                  onTap: () {
                    _auth.signOut();
                  },
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
