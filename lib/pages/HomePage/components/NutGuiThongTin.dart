import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class NutGuiThongTin extends StatefulWidget {
  final Function onPressed;
  NutGuiThongTin({Key key, this.onPressed}) : super(key: key);

  @override
  _NutGuiThongTinState createState() => _NutGuiThongTinState();
}

class _NutGuiThongTinState extends State<NutGuiThongTin> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/nut-gui-thong-tin-background.png"),
            fit: BoxFit.cover),
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(125)),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          splashColor: Color.fromRGBO(222, 81, 49, 0.1),
          borderRadius: BorderRadius.all(Radius.circular(125)),
          onTap: widget.onPressed != null ? widget.onPressed : () {},
          child: Padding(
            padding: const EdgeInsets.all(42.0),
            child: Icon(
              Icons.mail_outline,
              size: 85,
              color: primaryColor,
            ),
          ),
        ),
      ),
    );
  }
}
