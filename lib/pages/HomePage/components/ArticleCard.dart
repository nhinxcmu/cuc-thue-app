import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Article.dart';
import 'package:cuc_thue/pages/TinTucPage/TinTucPage.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

class ArticleCard extends StatelessWidget {
  static Widget loading() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          boxShadow: boxShadow,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Shimmer.fromColors(
                      baseColor: Colors.grey.withOpacity(0.4),
                      highlightColor: Colors.white,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: Colors.grey.withOpacity(0.4),
                            ),
                            height: 115,
                          ))
                        ],
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Shimmer.fromColors(
                        baseColor: Colors.grey.withOpacity(0.4),
                        highlightColor: Colors.white,
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.grey.withOpacity(0.4),
                          ),
                          height: 15,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Shimmer.fromColors(
                        baseColor: Colors.grey.withOpacity(0.4),
                        highlightColor: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.grey.withOpacity(0.4),
                          ),
                          height: 15,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Shimmer.fromColors(
                        baseColor: Colors.grey.withOpacity(0.4),
                        highlightColor: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.grey.withOpacity(0.4),
                          ),
                          height: 15,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Shimmer.fromColors(
                        baseColor: Colors.grey.withOpacity(0.4),
                        highlightColor: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.grey.withOpacity(0.4),
                          ),
                          height: 15,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Shimmer.fromColors(
                        baseColor: Colors.grey.withOpacity(0.4),
                        highlightColor: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.grey.withOpacity(0.4),
                          ),
                          height: 15,
                        )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  ArticleCard({
    Key key,
    this.article,
  }) : super(key: key);
  final Article article;
  ImageProvider getImage() {
    try {
      var image = NetworkImage(
        host + article.image,
      );
      return image;
    } catch (e) {
      return NetworkImage(
        host + '/dist/img/default-img.png',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          boxShadow: boxShadow,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.white,
        ),
        child: Column(
          children: <Widget>[
            Container(
              child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      String url =
                          host + '/#/tin-tuc/' + article.articleId.toString();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TinTucPage(
                                    url: url,
                                  )));
                    },
                  )),
              height: 115,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  image: DecorationImage(fit: BoxFit.cover, image: getImage())),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    article.title.trim(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                    ),
                  ),
                  Text(
                    formatter.format(article.date),
                    style: TextStyle(
                      fontSize: 13,
                      color: Color.fromRGBO(0, 0, 0, 0.3),
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  Text(
                    article.description.trim(),
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
