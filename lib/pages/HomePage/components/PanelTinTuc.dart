import 'dart:async';
import 'dart:convert';

import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Article.dart';
import 'package:flutter/material.dart';
import 'ArticleCard.dart';
import 'package:http/http.dart' as http;

class PanelTinTuc extends StatefulWidget {
  PanelTinTuc({Key key, this.duration}) : super(key: key);
  int duration = 5;
  final PageController pageController = PageController(
    viewportFraction: 0.8,
    initialPage: 0,
    keepPage: true,
  );
  @override
  _PanelTinTucState createState() => _PanelTinTucState();
}

class _PanelTinTucState extends State<PanelTinTuc> {
  int _curentPage = 0;
  List<Article> articles = [];

  void layDanhSachArticle() {
    var map = new Map<String, dynamic>();
    map['pageSize'] = '5';
    map['page'] = '0';
    map['categoryID'] = '1';
    map['startDate'] = 'Sun Jan 01 2017 00:00:00 GMT+0700 (Indochina Time)';
    map['endDate'] = 'Thu Feb 27 2020 14:57:15 GMT+0700 (Indochina Time)';
    map['searchString'] = '';
    http.post(host + '/article/get-list', body: map).then((res) {
      Map body = json.decode(res.body);
      List content = body['content'];
      setState(() {
        var _articles = content.map((map) => Article.fromMap(map)).toList();
        articles = _articles;
      });
    }).catchError((error){
      print(error);
    });
  }

  @override
  void initState() {
    super.initState();

    Timer.periodic(Duration(seconds: widget.duration), (Timer timer) {
      if (widget.pageController.hasClients) {
        int next = _curentPage + 1;
        if (next == articles.length + 1) next = 0;
        widget.pageController.animateToPage(next,
            duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
        setState(() {
          _curentPage = next;
        });
      }
    });
    layDanhSachArticle();
  }
  @override
  void dispose() {
    super.dispose();
    widget.pageController.dispose();
    
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 265,
      child: PageView(
        controller: widget.pageController,
        children: articles.length>0? articles.map((article) {
          return ArticleCard(
            article: article,
          );
        }).toList():[
          ArticleCard.loading(),
          ArticleCard.loading(),
          ArticleCard.loading()
        ],
      ),
    );
  }
}
