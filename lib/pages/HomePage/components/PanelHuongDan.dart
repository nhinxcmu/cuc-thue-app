import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';

class PanelHuongDan extends StatelessWidget {
  const PanelHuongDan({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.8 - 16;
    return Container(
      width: width,
      decoration: BoxDecoration(
          boxShadow: boxShadow,
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: primaryColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5), topRight: Radius.circular(5))),
            alignment: Alignment.center,
            width: width,
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Text("Gửi thông tin",
                    style: TextStyle(
                        fontSize: 17, letterSpacing: 2, color: Colors.white)),
                Text("ngay khi phát hiện",
                    style: TextStyle(
                        fontSize: 17, letterSpacing: 2, color: Colors.white)),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5),
                    bottomRight: Radius.circular(5))),
            alignment: Alignment.center,
            height: 70,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Icon(
                  Icons.warning,
                  size: 75,
                  color: primaryColor.withOpacity(0.3),
                ),
                Text("VI PHẠM",
                    style: TextStyle(
                      fontSize: 35,
                      letterSpacing: 2,
                      fontWeight: FontWeight.bold,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
