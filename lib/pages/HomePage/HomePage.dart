import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Mail.dart';
import 'package:cuc_thue/pages/CamOnPage/CamOn.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/DanhSachTinNhanPage.dart';
import 'package:cuc_thue/pages/DocTinNhanPage/DocTinNhanPage.dart';
import 'package:cuc_thue/pages/GuiThongTinPage/GuiThongTinPage.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'components/PanelUser.dart';
import 'components/NutGuiThongTin.dart';
import 'components/PanelHuongDan.dart';
import 'components/PanelTinTuc.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  print(message);
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}

class HomePage extends StatefulWidget {
  AnimationController _animationController;
  HomePage({Key key}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FirebaseUser currentUser;
  SharedPreferences prefs;

  Future onDidReceiveLocalNotification(int a, String s, String d, String f) {
    print([a, s, d, f]);
  }

  Future onSelectNotification(String payload) {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
  }

  void initLocalNotification() async {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  void guiLocalNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        '1', 'thuecamau', 'Thuế Cà Mau' 's channel',
        groupKey: "1", importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    if (message["data"]["mid"] != null)
      await flutterLocalNotificationsPlugin.show(
        int.parse(message["data"]["mid"]),
        message["notification"]["title"],
        message["notification"]["body"],
        platformChannelSpecifics,
        payload: 'Default_Sound',
      );
    else
      await flutterLocalNotificationsPlugin.show(
        1,
        message["notification"]["title"],
        message["notification"]["body"],
        platformChannelSpecifics,
        payload: 'Default_Sound',
      );
  }

  void firebaseMessageInit() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        guiLocalNotification(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        Mail mail = Mail(
            mailId: int.parse(message['data']['pmid']),
            subject: message['data']['pmSubject']);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DocTinNhanPage(mail: mail)));
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        Mail mail = Mail(
            mailId: int.parse(message['data']['pmid']),
            subject: message['data']['pmSubject']);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DocTinNhanPage(mail: mail)));
      },
      onBackgroundMessage: myBackgroundMessageHandler,
    );
    _firebaseMessaging.onTokenRefresh.listen((token) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString("token", token);
    });
    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(
    //         sound: true, badge: true, alert: true, provisional: true));

    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   print("Settings registered: $settings");
    // });
    subscribeTopicGlobal();
  }

  void layUserId(sdt) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = await prefs.getString("token");
    if (token == null) token = await _firebaseMessaging.getToken();
    var uri = Uri.parse(host + '/unauthenticated/check-phone-number');

    var request = http.MultipartRequest('POST', uri);
    String authorization =
        "Basic " + base64Encode(utf8.encode('defuser:Cmu#2018'));
    request.headers["Authorization"] = authorization;

    request.fields["sdt"] = sdt;
    request.fields["deviceId"] = token;

    var res = await request.send();
    Map response = json.decode(await res.stream.bytesToString());
    int userId = response["code"];
    print("userid:" + userId.toString());
    await luuUserIdVaoSharedPreference(userId);
  }

  Future<void> luuUserIdVaoSharedPreference(int userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("userId", userId);
  }

  void chuyenTrangGuiThongTin() {
    widget._animationController.reverse().whenComplete(() async {
      await Navigator.push(
          context,
          PageRouteBuilder(
              pageBuilder: (context, animation1, animation2) =>
                  GuiThongTinPage(),
              maintainState: false));
      widget._animationController.forward();
    });
  }

  subscribeTopicGlobal() {
    _firebaseMessaging.subscribeToTopic('global').then((f) {
      print("Đã subscribeToTopic global");
    }).catchError((error) {
      print(error);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    firebaseMessageInit();
    initLocalNotification();
    _firebaseMessaging.getToken();
    widget._animationController =
        AnimationController(vsync: this, duration: fadeUpListDuration);
    _auth.currentUser().then((user) {
      currentUser = user;
      layUserId(user.phoneNumber);
    });
  }

  @override
  void didUpdateWidget(HomePage oldWidget) {
    print(oldWidget._animationController.status);
    if (oldWidget._animationController.status == AnimationStatus.dismissed) {
      widget._animationController.forward();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Background(),
        FadeUpListView(
          animationController: widget._animationController,
          offsetY: -60,
          whenToDoNextAnimation: 0.3,
          children: <Widget>[
            PanelUser(
              onMailBoxTap: () {
                widget._animationController.reverse().then((e) async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DanhSachTinNhanPage()));
                  widget._animationController.forward();
                });
              },
            ),
            NutGuiThongTin(
              onPressed: chuyenTrangGuiThongTin,
            ),
            PanelHuongDan(),
            PanelTinTuc(
              duration: 10,
            )
          ],
        ),
      ]),
    );
  }
}

class Background extends StatelessWidget {
  const Background({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Hero(
            tag: "background",
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/background.png"),
                      fit: BoxFit.fitWidth,
                      alignment: Alignment.topCenter)),
            ),
          ),
        )
      ],
    );
  }
}
