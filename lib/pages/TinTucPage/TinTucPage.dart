import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TinTucPage extends StatefulWidget {
  TinTucPage({
    @required this.url,
  });
  String url;
  @override
  _TinTucPageState createState() => _TinTucPageState();
}

class _TinTucPageState extends State<TinTucPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(
        initialUrl: widget.url,
        javascriptMode: JavascriptMode.unrestricted,
        debuggingEnabled: false,
      ),
    );
  }
}
