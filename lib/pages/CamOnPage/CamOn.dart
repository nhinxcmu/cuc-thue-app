import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/pages/HomePage/HomePage.dart';
import 'package:cuc_thue/pages/LoginPage/LoginPage.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';

class CamOnPage extends StatefulWidget {
  @override
  _CamOnPageState createState() => _CamOnPageState();
}

class _CamOnPageState extends State<CamOnPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: fadeUpListDuration);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double vh = MediaQuery.of(context).size.height / 100;
    double vw = MediaQuery.of(context).size.width / 100;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: FadeUpListView(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              animationController: _controller,
              whenToDoNextAnimation: 0.3,
              offsetY: -60,
              children: [
                Center(
                  child: Logo(
                    vh: vh,
                    vw: vw,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 32, bottom: 32),
                  child: Center(
                    child: Text(
                      "Cảm ơn!",
                      style: TextStyle(
                          fontSize: 50,
                          color: Color.fromRGBO(0, 0, 0, 0.8),
                          letterSpacing: 2),
                    ),
                  ),
                ),
                RichText(
                  text: TextSpan(
                    style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      letterSpacing: 2,
                      fontStyle: FontStyle.italic,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Cục thuế Cà Mau',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: primaryColor),
                      ),
                      TextSpan(
                        text:
                            " xin chân thành cảm ơn thông tin đóng góp của bạn!",
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromRGBO(0, 0, 0, 0.8),
                          letterSpacing: 2,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                  child: Text(
                    "Thông tin của bạn đã được chúng tôi ghi nhận và trong quá trình xử lý.",
                    style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      letterSpacing: 2,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                  child: Text(
                    "Chúng tôi sẽ phúc đáp đến bạn sớm nhất có thể.",
                    style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      letterSpacing: 2,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 32.0),
                  child: Text(
                    "Chúc bạn có một ngày tốt lành!",
                    style: TextStyle(
                      fontSize: 16,
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      letterSpacing: 2,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 32),
                  child: Center(
                    child: RaisedButton(
                      disabledColor: Colors.grey,
                      // focusNode: focusNode,
                      splashColor: Color.fromRGBO(255, 255, 255, 0.5),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5.0))),
                      padding: EdgeInsets.only(
                          top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
                      color: primaryColor,
                      onPressed: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => HomePage()));
                      },
                      child: Text(
                        'Trang chủ',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22,
                            letterSpacing: 2),
                      ),
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
