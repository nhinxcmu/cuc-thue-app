import 'package:cuc_thue/pages/LoginPage/LoginPage.dart';
import 'package:cuc_thue/pages/LoginPage/components/FormDangNhap.dart';
import 'package:cuc_thue/pages/XacNhanPage/components/FormXacNhan.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';

class XacNhanPage extends StatelessWidget {
  final String sdt;
  final String verificationId;
  XacNhanPage({
    @required this.sdt,
    @required this.verificationId,
  });
  @override
  Widget build(BuildContext context) {
    double vh = MediaQuery.of(context).size.height / 100;
    double vw = MediaQuery.of(context).size.width / 100;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 95 * vh,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Hero(tag: "logo", child: Logo(vh: vh, vw: vw)),
                    Padding(
                      padding: EdgeInsets.only(top: 5 * vh),
                      child: FormXacNhan(
                        sdt: sdt,
                        verificationId: verificationId,
                      ),
                    ),
                  ],
                ),
              ),
              BanQuyen()
            ],
          ),
        ),
      ),
    );
  }
}
