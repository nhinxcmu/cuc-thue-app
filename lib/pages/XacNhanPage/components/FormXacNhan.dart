import 'dart:async';

import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/main.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:cuc_thue/pages/HomePage/HomePage.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class FormXacNhan extends StatefulWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FormXacNhan({
    @required this.sdt,
    @required this.verificationId,
  });
  final String sdt;
  final String verificationId;
  Duration duration = Duration(seconds: 1);
  @override
  _FormXacNhanState createState() => _FormXacNhanState();
}

class _FormXacNhanState extends State<FormXacNhan>
    with SingleTickerProviderStateMixin {
  String smsCode;
  PhoneCodeSent _codeSent;
  PhoneVerificationCompleted _verificationCompleted;
  PhoneVerificationFailed _verificationFailed;
  PhoneCodeAutoRetrievalTimeout _codeAutoRetrievalTimeout;
  String verificationId;

  AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(vsync: this, duration: fadeUpListDuration);
    verificationId = widget.verificationId;
    _codeSent = (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Đã gửi lại mã xác nhận."),
      ));
    };
    _verificationCompleted = (AuthCredential credential) {
      widget._auth.signInWithCredential(credential).then((value) {
        print(value.user);
        Navigator.pushReplacement(
            context, new MaterialPageRoute(builder: (context) => HomePage()));
      }).catchError((error) {
        print(error);
      });
      setState(() {
        print("verificationCompleted");
      });
    };

    _verificationFailed = (AuthException authException) {
      setState(() {
        print(
            'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
      });
    };

    _codeAutoRetrievalTimeout = (String verificationId) {
      print("time out");
    };
    super.initState();
  }

  void maXacNhanInputChange(String value) {
    setState(() {
      smsCode = value;
    });
  }

  void xacNhan() {
    var credential = PhoneAuthProvider.getCredential(
        verificationId: this.verificationId, smsCode: smsCode);
    widget._auth.signInWithCredential(credential).then((result) {
      if (result.user != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      }
    }).catchError((error) {
      print(error);
      if (error.code == 'ERROR_INVALID_VERIFICATION_CODE') {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Sai mã xác nhận, vui lòng thử lại!"),
        ));
      }
      if (error.code == 'ERROR_SESSION_EXPIRED') {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Mã xác nhận hết hạn!"),
        ));
      }
      //'ERROR_INVALID_VERIFICATION_CODE'
      //ERROR_SESSION_EXPIRED
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: FadeUpListView(
          animationController: _animationController,
          offsetY: -60,
          whenToDoNextAnimation: 0.3,
          children: [
            Text(
              "Mã xác nhận đã được gửi!",
              style: TextStyle(
                  fontSize: 22,
                  color: Color.fromRGBO(0, 0, 0, 0.8),
                  letterSpacing: 2),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Nhập mã xác nhận được gửi đến ",
                  style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 0.8),
                    fontStyle: FontStyle.italic,
                  ),
                ),
                Text(
                  widget.sdt,
                  style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 0.8),
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(22.0),
              child: PinCodeTextField(
                textInputType: TextInputType.number,
                length: 6,
                obsecureText: false,
                animationType: AnimationType.fade,
                shape: PinCodeFieldShape.box,
                animationDuration: Duration(milliseconds: 300),
                borderRadius: BorderRadius.circular(5),
                fieldHeight: 50,
                fieldWidth: 40,
                onChanged: maXacNhanInputChange,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Không nhận được mã xác nhận?",
                    style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  NutGuiLai(sdt: widget.sdt),
                ],
              ),
            ),
            RaisedButton(
              padding: EdgeInsets.only(
                  top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
              color: primaryColor,
              onPressed: () {
                if (smsCode.length == 6) {
                  xacNhan();
                } else
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Mã xác nhận gồm 6 ký tự!"),
                  ));
              },
              child: Text(
                'Xác nhận',
                style: TextStyle(
                    color: Colors.white, fontSize: 22, letterSpacing: 2),
              ),
            ),
          ]),
    );
  }
}

class NutGuiLai extends StatefulWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  NutGuiLai({
    this.sdt,
  });
  String sdt = "";
  @override
  _NutGuiLaiState createState() => _NutGuiLaiState();
}

class _NutGuiLaiState extends State<NutGuiLai> {
  PhoneCodeSent _codeSent;
  PhoneVerificationCompleted _verificationCompleted;
  PhoneVerificationFailed _verificationFailed;
  PhoneCodeAutoRetrievalTimeout _codeAutoRetrievalTimeout;
  int countDown = 60;
  Timer _timer;
  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    _codeSent = (String verificationId, [int forceResendingToken]) async {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Đã gửi lại mã xác nhận."),
      ));
      setState(() {
        countDown = 60;
      });
      _timer=Timer.periodic(new Duration(seconds: 1), (timer) {
        if (countDown > 0)
          setState(() {
            countDown--;
          });
        else
          timer.cancel();
      });
    };
    _verificationCompleted = (AuthCredential credential) {
      widget._auth.signInWithCredential(credential).then((value) {
        print(value.user);
        Navigator.pushReplacement(
            context, new MaterialPageRoute(builder: (context) => HomePage()));
      }).catchError((error) {
        print(error);
      });
      setState(() {
        print("verificationCompleted");
      });
    };

    _verificationFailed = (AuthException authException) {
      setState(() {
        print(
            'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
      });
    };

    _codeAutoRetrievalTimeout = (String verificationId) {
      print("time out");
    };
    _timer = Timer.periodic(new Duration(seconds: 1), (timer) {
      if (countDown > 0)
        setState(() {
          countDown = countDown - 1;
        });
      else
        timer.cancel();
    });
    super.initState();
  }

  void guiMaXacNhan() async {
    if (countDown > 0)
      Scaffold.of(context).showSnackBar(SnackBar(
          content:
              Text("Vui lòng thử lại sau " + countDown.toString() + " giây")));
    else {
      await widget._auth.verifyPhoneNumber(
          phoneNumber: widget.sdt,
          timeout: Duration(seconds: 60),
          verificationCompleted: _verificationCompleted,
          verificationFailed: _verificationFailed,
          codeSent: _codeSent,
          codeAutoRetrievalTimeout: _codeAutoRetrievalTimeout);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.transparent,
        child: InkWell(
          child: Container(
              padding: EdgeInsets.all(8.0),
              color: Colors.transparent,
              child: Text(
                "GỬI LẠI" +
                    (countDown > 0 ? " (" + countDown.toString() + "s)" : ""),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                  color: primaryColor,
                ),
              )),
          onTap: guiMaXacNhan,
        ));
  }
}
