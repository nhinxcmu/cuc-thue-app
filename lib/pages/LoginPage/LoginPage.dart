import 'package:cuc_thue/pages/LoginPage/components/FormDangNhap.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    double vh = MediaQuery.of(context).size.height / 100;
    double vw = MediaQuery.of(context).size.width / 100;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 95 * vh,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Hero(tag: "logo", child: Logo(vh: vh, vw: vw)),
                    Padding(
                      padding: EdgeInsets.only(top: 5 * vh),
                      child: FormDangNhap(),
                    ),
                  ],
                ),
              ),
              BanQuyen()
            ],
          ),
        ),
      ),
    );
  }
}

 class Logo extends StatelessWidget {
  const Logo({
    Key key,
    @required this.vh,
    @required this.vw,
    this.padding
  }) : super(key: key);

  final double vh;
  final double vw;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:this.padding==null? EdgeInsets.only(top: 10 * vh): padding,
      child: Container(
        width: 50 * vw,
        height: 50 * vw,
        decoration: BoxDecoration(
            boxShadow: boxShadow,
            borderRadius: BorderRadius.all(Radius.circular(125)),
            image: DecorationImage(image: AssetImage("assets/logo.png"))),
      ),
    );
  }
}

class BanQuyen extends StatelessWidget {
  const BanQuyen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double vh = MediaQuery.of(context).size.height / 100;
    double vw = MediaQuery.of(context).size.width / 100;
    return Container(
      height: 5 * vh,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Bản quyền thuộc",
                style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 0.8),
                    letterSpacing: 2,
                    fontStyle: FontStyle.italic,
                    fontSize: 12),
              ),
              Text(
                " Cục Thuế Tỉnh Cà Mau",
                style: TextStyle(
                    color: primaryColor,
                    letterSpacing: 2,
                    fontStyle: FontStyle.italic,
                    fontSize: 12),
              ),
            ],
          ),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: <Widget>[
          //     Text(
          //       "Thiết kế và phát triển bởi",
          //       style: TextStyle(
          //           color: Color.fromRGBO(0, 0, 0, 0.8),
          //           letterSpacing: 2,
          //           fontStyle: FontStyle.italic,
          //           fontSize: 12),
          //     ),
          //     Text(
          //       " VNPT Cà Mau",
          //       style: TextStyle(
          //           color: Colors.blue,
          //           letterSpacing: 2,
          //           fontStyle: FontStyle.italic,
          //           fontSize: 12),
          //     ),
          //   ],
          // ),
        ],
      ),
    );
  }
}
