import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/main.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:cuc_thue/pages/XacNhanPage/XacNhanPage.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:cuc_thue/pages/HomePage/HomePage.dart';
import 'package:firebase_auth/firebase_auth.dart';

// Define a custom Form widget.
class FormDangNhap extends StatefulWidget {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  FormDangNhapState createState() {
    return FormDangNhapState();
  }
}

class FormDangNhapState extends State<FormDangNhap>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  PhoneCodeSent _codeSent;
  PhoneVerificationCompleted _verificationCompleted;
  PhoneVerificationFailed _verificationFailed;
  PhoneCodeAutoRetrievalTimeout _codeAutoRetrievalTimeout;
  String sdt = "";
  String verificationId;
  String smsCode = "";
  bool secondAnimationStart = false;
  bool thirdAnimationStart = false;
  bool forthAnimationStart = false;
  AnimationController _animationController;
  bool disabled = false;

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: fadeUpListDuration);
    _codeSent = (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      print("code sent to " + sdt);
      _animationController.reverse().whenComplete(() async {
        await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    XacNhanPage(sdt: sdt, verificationId: verificationId)));
        _animationController.forward();
      });
    };

    _verificationCompleted = (AuthCredential credential) {
      widget._auth.signInWithCredential(credential).then((value) {
        print(value.user);
        Navigator.pushReplacement(
            context, new MaterialPageRoute(builder: (context) => HomePage()));
      }).catchError((error) {
        print(error);
      });
      setState(() {
        print("verificationCompleted");
      });
    };

    _verificationFailed = (AuthException authException) {
      setState(() {
        print(
            'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
      });
    };

    _codeAutoRetrievalTimeout = (String verificationId) {
      this.verificationId = verificationId;
      print("time out");
    };

    super.initState();
  }

  Function inputChange(String value) {
    setState(() {
      sdt = value;
    });
  }

  void guiMaXacNhan() async {
    setState(() {
      disabled = true;
    });
    if (sdt.startsWith("0")) {
      sdt = sdt.replaceRange(0, 1, "+84");
    } else if (sdt.startsWith("+84")) {
      //DO NOTHING
    } else
      sdt = "+84" + sdt;
    await widget._auth.verifyPhoneNumber(
      phoneNumber: sdt,
      timeout: Duration(seconds: 60),
      verificationCompleted: _verificationCompleted,
      verificationFailed: _verificationFailed,
      codeSent: _codeSent,
      codeAutoRetrievalTimeout: _codeAutoRetrievalTimeout,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 50),
        child: Form(
          key: _formKey,
          child: FadeUpListView(
              animationController: _animationController,
              offsetY: -60,
              whenToDoNextAnimation: 0.3,
              children: [
                Text(
                  "Xin chào!",
                  style: TextStyle(
                      fontSize: 50,
                      color: Color.fromRGBO(0, 0, 0, 0.8),
                      letterSpacing: 2),
                ),
                Text(
                  "Hãy nhập số điện thoại để đăng nhập",
                  style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 0.8),
                    letterSpacing: 2,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    onChanged: inputChange,
                    cursorColor: Color.fromRGBO(0, 0, 0, 0.8),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25,
                    ),
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.phone_android),
                      prefixText: "+84",
                      contentPadding: EdgeInsets.all(8.0),
                      border: OutlineInputBorder(),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Không được để trống!';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  disabledColor: Colors.grey,
                  disabledElevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  padding: EdgeInsets.only(
                      top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
                  color: primaryColor,
                  onPressed: disabled
                      ? null
                      : () {
                          if (_formKey.currentState.validate()) {
                            guiMaXacNhan();
                          }
                        },
                  child: Text(
                    'Gửi mã xác nhận',
                    style: TextStyle(
                        color: Colors.white, fontSize: 22, letterSpacing: 2),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}
