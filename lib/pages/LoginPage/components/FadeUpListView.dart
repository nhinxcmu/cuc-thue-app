import 'package:flutter/material.dart';

class FadeUpListView extends StatefulWidget {
  final List<Widget> children;
  final double whenToDoNextAnimation;
  final int offsetY;
  final AnimationController animationController;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;

  const FadeUpListView(
      {Key key,
      @required this.whenToDoNextAnimation,
      @required this.offsetY,
      @required this.children,
      this.animationController,
      this.mainAxisAlignment,
      this.crossAxisAlignment})
      : super(
          key: key,
        );
  @override
  _FadeUpListViewState createState() => _FadeUpListViewState();
}

class _FadeUpListViewState extends State<FadeUpListView>
    with TickerProviderStateMixin {
  List<Animation> _animations = new List();
  @override
  void didUpdateWidget(FadeUpListView oldWidget) {
    super.didUpdateWidget(oldWidget);
    // List<Widget> newChildren = widget.children.removeWhere((n)=>oldWidget.children.contains(n));
    List<Widget> newChildren =
        widget.children.sublist(oldWidget.children.length);
    if (newChildren.length > 0) {
      AnimationController animationController = AnimationController(
          vsync: this,
          duration: Duration(milliseconds: 200 * newChildren.length));
      _animations.addAll(themAnimations(
          animationController.duration.inMilliseconds,
          0.8,
          -60,
          newChildren,
          animationController));
      animationController.addListener(() {
        setState(() {});
      });
      animationController.forward().then((f) {
        animationController.dispose();
      });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    widget.animationController.addListener(() {
      setState(() {});
    });
    _animations.addAll(themAnimations(
        widget.animationController.duration.inMilliseconds,
        widget.whenToDoNextAnimation,
        widget.offsetY,
        widget.children,
        widget.animationController));
    widget.animationController.forward();
  }

  List<Animation> themAnimations(
      int durationInMiliseconds,
      double whenToDoNextAnimation,
      int offsetY,
      List<Widget> children,
      AnimationController animationController) {
    int thoiGianTong =
        durationInMiliseconds; //widget.animationController.duration.inMilliseconds;
    double thoiGianCon = thoiGianTong / children.length;
    double thoiGianDu =
        (1 - whenToDoNextAnimation) * //widget.whenToDoNextAnimation) *
            children.length *
            thoiGianCon;
    double thoiGianConSauKhiCongThoiGianDu =
        thoiGianCon + thoiGianDu / children.length;

    double thoiGianBatDauTruocDo = 0;
    List<Animation> animations = [];
    for (var i = 0; i < children.length; i++) {
      double thoiGianBatDau = i != 0
          ? thoiGianBatDauTruocDo +
              thoiGianConSauKhiCongThoiGianDu * whenToDoNextAnimation
          : 0;
      double thoiGianKetThuc = thoiGianBatDau + thoiGianConSauKhiCongThoiGianDu;
      double begin = thoiGianBatDau / thoiGianTong;
      double end =
          i != children.length - 1 ? thoiGianKetThuc / thoiGianTong : 1;
      animations.add(new Tween<double>(begin: 0, end: 1).animate(
          new CurvedAnimation(
              parent: animationController, curve: Interval(begin, end))));
      thoiGianBatDauTruocDo = thoiGianBatDau;
    }
    if (offsetY > 0) animations = animations.reversed.toList();
    return animations;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: widget.mainAxisAlignment == null
          ? MainAxisAlignment.spaceBetween
          : widget.mainAxisAlignment,
      crossAxisAlignment: widget.crossAxisAlignment == null
          ? CrossAxisAlignment.center
          : widget.crossAxisAlignment,
      children: widget.children.map((child) {
        Animation _animation = _animations[widget.children.indexOf(child)];
        return Opacity(
          opacity: _animation.value,
          child: Transform.translate(
            offset: Offset(0, -widget.offsetY * (1 - _animation.value)),
            child: child,
          ),
        );
      }).toList(),
    );
  }

  @override
  void dispose() {
    // widget.animationController.dispose();
    super.dispose();
  }
}
