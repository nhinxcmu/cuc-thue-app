import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class XemHinhAnhPage extends StatefulWidget {
  List<String> urls;
  int initIndex = 0;
  XemHinhAnhPage({
    this.urls,
    this.initIndex,
  });
  @override
  _XemHinhAnhPageState createState() => _XemHinhAnhPageState();
}

class _XemHinhAnhPageState extends State<XemHinhAnhPage> {
  PageController _pageController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(initialPage: widget.initIndex);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child: PhotoViewGallery.builder(
      pageController: _pageController,
      scrollPhysics: const BouncingScrollPhysics(),
      builder: (BuildContext context, int index) {
        return PhotoViewGalleryPageOptions(
          imageProvider: NetworkImage(widget.urls[index].replaceAll("THM", "ORG")),
          initialScale: PhotoViewComputedScale.contained,
          heroAttributes: PhotoViewHeroAttributes(tag: widget.urls[index]),
        );
      },
      itemCount: widget.urls.length,
      loadingBuilder: (context, event) => Center(
        child: Container(
          width: 20.0,
          height: 20.0,
          child: CircularProgressIndicator(
            value: event == null
                ? 0
                : event.cumulativeBytesLoaded / event.expectedTotalBytes,
          ),
        ),
      ),
    ));
  }
}
