import 'dart:convert';

import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Mail.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/components/TinNhanCard.dart';
import 'package:cuc_thue/pages/GuiThongTinPage/GuiThongTinPage.dart';
import 'package:cuc_thue/pages/HomePage/HomePage.dart';
import 'package:cuc_thue/pages/HomePage/components/PanelUser.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class DanhSachTinNhanPage extends StatefulWidget {
  @override
  _DanhSachTinNhanPageState createState() => _DanhSachTinNhanPageState();
}

class _DanhSachTinNhanPageState extends State<DanhSachTinNhanPage>
    with TickerProviderStateMixin {
  SharedPreferences prefs;
  AnimationController _controller;
  List<AnimationController> _controllers = [];
  ScrollController _scrollController;
  int _page = 0;
  bool last = false;

  List<List<Mail>> mailsList = [];
  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: fadeUpListDuration);
    _controllers.add(_controller);
    _scrollController = ScrollController(keepScrollOffset: true)
      ..addListener(() {
        if (_scrollController.position.pixels ==
                _scrollController.position.maxScrollExtent &&
            !last) {
          _page++;
          layDsTinNhan();
        }
      });
    layDsTinNhan();
  }

  @override
  void dispose() {
    super.dispose();
    _controllers.forEach((controller) {
      controller.dispose();
    });
    _scrollController.dispose();
  }

  Future<bool> _onWillPop() async {
    await _controller.reverse();
    return true;
  }

  void layDsTinNhan() async {
    prefs = await SharedPreferences.getInstance();

    var map = new Map<String, dynamic>();
    map['uid'] = prefs.getInt("userId").toString();
    map['page'] = _page.toString();

    var headers = new Map<String, String>();
    String authorization =
        "Basic " + base64Encode(utf8.encode('defuser:Cmu#2018'));
    headers["Authorization"] = authorization;

    http
        .post(host + '/mobile/lay-ds-tin-nhan', body: map, headers: headers)
        .then((res) {
      Map body = json.decode(res.body);
      bool _last = body['last'];
      List content = body['content'];
      var mails = content.map((map) => Mail.fromMap(map)).toList();
      setState(() {
        last = _last;
        mailsList.add(mails);
      });
    }).catchError((error) {
      print(error);
    });
  }

  SliverFixedExtentList getTinNhanCards(List<Mail> mails) {
    var controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: mails.length * 200));
    _controllers.add(controller);
    return SliverFixedExtentList(
      itemExtent: mails.length * 155.0,
      delegate: SliverChildListDelegate(
        [
          FadeUpListView(
              mainAxisAlignment: MainAxisAlignment.start,
              animationController: controller,
              whenToDoNextAnimation: 0.8,
              offsetY: -60,
              children: mails
                  .map((mail) => TinNhanCard(
                        onTap: () {
                          // var _mailList =mailsList;
                          // _mailList[_mailList.indexOf(mails)][mails.indexOf(mail)].seen=true;
                          mail.seen=true;
                          setState(() {
                          });
                        },
                        mail: mail,
                      ))
                  .toList())
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Stack(children: [
        Background(),
        Scaffold(
          backgroundColor: Colors.transparent,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => GuiThongTinPage()));
            },
            child: Icon(
              Icons.add,
              color: primaryColor,
            ),
          ),
          body: CustomScrollView(
            physics: const BouncingScrollPhysics(),
            controller: _scrollController,
            slivers: <Widget>[
              SliverPadding(
                padding: const EdgeInsets.only(bottom: 40),
                sliver: SliverAppBar(
                  backgroundColor: Colors.transparent,
                  expandedHeight: 220.0,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text(
                      "Tin báo",
                      style: TextStyle(
                          fontSize: 40,
                          color: Color.fromRGBO(0, 0, 0, 0.7),
                          letterSpacing: 2),
                    ),
                    centerTitle: true,
                  ),
                ),
              ),
            ]
              ..addAll(mailsList.map((mailList) => getTinNhanCards(mailList)))
              ..add(SliverPadding(
                padding: EdgeInsets.only(bottom: last ? 0 : 400),
              )),
          ),
        ),
        PanelUser(
          onMailBoxTap: null,
        ),
      ]),
    );
  }
}
