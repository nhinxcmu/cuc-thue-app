import 'dart:io';

import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Attachment.dart';
import 'package:cuc_thue/models/Mail.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/components/ListHinhAnh.dart';
import 'package:cuc_thue/pages/DocTinNhanPage/DocTinNhanPage.dart';
import 'package:cuc_thue/pages/XemHinhAnhPage/XemHinhAnhPage.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class TinNhanCard extends StatelessWidget {
  TinNhanCard({
    @required this.mail,
    this.onTap,
    Key key,
  }) : super(key: key);
  final Mail mail;
  final Function onTap;
  String getMap() {
    String url = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/" +
        mail.lon.toString() +
        "," +
        mail.lat.toString() +
        ",15/280x200?access_token=pk.eyJ1IjoibmhpY2FtYXU1MiIsImEiOiJjazd5NnhwMzkwM3FwM29td2s3cHI3Y3F5In0.RdV8OgO9MEXyKFVBf55gAg";
    return url;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
      child: Material(
        color: Colors.white,
        elevation: 4,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: InkWell(
          hoverColor: Colors.white,
          onTap: () async {
            await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DocTinNhanPage(mail: mail)));
            if (onTap != null) onTap();
          },
          child: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 100,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 100,
                        height: 75,
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(8.0)),
                          image: DecorationImage(
                              image: NetworkImage(getMap()), fit: BoxFit.cover),
                        ),
                        child: Center(
                            child: Container(
                          width: 8,
                          height: 8,
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(4),
                          ),
                        )),
                      ),
                      Container(
                        child: Text(
                          formatter.format(mail.sendDate),
                          style: TextStyle(
                              color: secondaryColor.withOpacity(0.8),
                              fontStyle: FontStyle.italic),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.image,
                                  color: Color.fromRGBO(0, 0, 0, 0.8),
                                ),
                                Text(
                                  mail.attachments.length.toString(),
                                  style: TextStyle(
                                    color: secondaryColor.withOpacity(0.8),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.message,
                                  color: Color.fromRGBO(0, 0, 0, 0.8),
                                ),
                                Text(
                                  mail.childCount.toString(),
                                  style: TextStyle(
                                    color: secondaryColor.withOpacity(0.8),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  mail.subject,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              // if (!mail.seen)
                              //   Container(
                              //     height: 10,
                              //     width: 10,
                              //     decoration: BoxDecoration(
                              //       color: Colors.red,
                              //       borderRadius: BorderRadius.circular(5),
                              //     ),
                              //   )
                            ],
                          ),
                          if(mail.attachments.length>0) ListHinhAnh(
                            urls: mail.attachments
                                .map(
                                    (attachment) => host + "/" + attachment.url)
                                .toList(),
                          ),
                        ]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
