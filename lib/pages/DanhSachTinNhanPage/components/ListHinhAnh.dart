import 'package:cuc_thue/pages/XemHinhAnhPage/XemHinhAnhPage.dart';
import 'package:flutter/material.dart';

class ListHinhAnh extends StatelessWidget {
  const ListHinhAnh({
    Key key,
    @required this.urls,
  }) : super(key: key);

  final List<String> urls;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Color.fromRGBO(219, 219, 219, 1),
        ),
        height: 100,
        child: urls.length > 0
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: urls.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.only(
                        top: 8, bottom: 8, right: 8, left: index == 0 ? 8 : 0),
                    child: Hero(
                      tag: urls[index],
                      child: Container(
                        width: 100,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            image: DecorationImage(
                              image: NetworkImage(urls[index]),
                              fit: BoxFit.cover,
                            )),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            splashColor: Colors.white.withOpacity(0.3),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => XemHinhAnhPage(
                                    urls: urls,
                                    initIndex: index,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  );
                },
              )
            : Container(
                alignment: Alignment.center,
                height: 140,
                child: Text(
                  "Chưa có hình ảnh",
                  style: TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 0.5),
                      fontStyle: FontStyle.italic),
                ),
              ),
      ),
    );
  }
}
