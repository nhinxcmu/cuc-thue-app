import 'dart:convert';
import 'dart:io';

import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Mail.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/components/TinNhanCard.dart';
import 'package:cuc_thue/pages/DocTinNhanPage/components/TinNhanBubble.dart';
import 'package:cuc_thue/pages/HomePage/HomePage.dart';
import 'package:cuc_thue/pages/LoginPage/components/FadeUpListView.dart';
import 'package:cuc_thue/services/MobileService.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class DocTinNhanPage extends StatefulWidget {
  Mail mail;
  DocTinNhanPage({@required this.mail});
  @override
  _DocTinNhanPageState createState() => _DocTinNhanPageState();
}

class _DocTinNhanPageState extends State<DocTinNhanPage>
    with TickerProviderStateMixin {
  AnimationController _controller;
  ScrollController _scrollController;
  TextEditingController _textEditingController;

  Location location = new Location();
  LocationData _locationData;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  SharedPreferences prefs;
  MobileService mobileService = MobileService();

  List<Mail> mails = [];
  List<File> files = [];
  String content = "";
  bool sending = false;

  @override
  void initState() {
    super.initState();

    _scrollController =
        ScrollController(initialScrollOffset: 0, keepScrollOffset: true);
    _textEditingController = TextEditingController();
    layDsTinNhan();
    layViTri();
  }

  @override
  void didUpdateWidget(DocTinNhanPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    _scrollController.animateTo(0,
        duration: Duration(milliseconds: 100), curve: Curves.easeOut);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  Future<void> layViTri() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.GRANTED) {
        return;
      }
    }

    _locationData = await location.getLocation();
  }

  void layDsTinNhan() async {
    // prefs.getInt("userId").toString();
    //widget.mail.mailId.toString();
    mobileService.docTinNhan(widget.mail.mailId, (_mails) {
      _controller = AnimationController(
          vsync: this, duration: Duration(milliseconds: 100 * _mails.length));
      setState(() {
        mails = _mails;
      });
    });
  }

  Future getImageFromCamera() async {
    // FocusScope.of(context).unfocus(focusPrevious: true);
    var file = await ImagePicker.pickImage(source: ImageSource.camera);
    if (file != null) {
      setState(() {
        files.add(file);
      });
    }
  }

  Future getImageFromFile() async {
    // FocusScope.of(context).unfocus(focusPrevious: true);
    var file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file != null) {
      setState(() {
        files.add(file);
      });
    }
  }

  guiTinNhan() {
    setState(() {
      sending = true;
    });
    mobileService.luuTinNhan(
        context,
        files,
        "Trao đổi tiếp: " + widget.mail.subject,
        content,
        1,
        widget.mail.mailId,
        _locationData, (res) {
      if (res.statusCode == 200) {
        layDsTinNhan();
        setState(() {
          files = [];
          content = "";
          sending = false;
        });
        _textEditingController.clear();
      } else
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Có lỗi khi gửi thông tin, vui lòng thử lại!"),
        ));
    });
  }

  @override
  Widget build(BuildContext context) {
    double vh = MediaQuery.of(context).size.height / 100;
    return Scaffold(
        body: Stack(
      children: [
        Background(),
        Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: mails.length > 0
                    ? SingleChildScrollView(
                        reverse: true,
                        physics: BouncingScrollPhysics(),
                        controller: _scrollController,
                        child: FadeUpListView(
                          mainAxisAlignment: MainAxisAlignment.start,
                          animationController: _controller,
                          offsetY: 60,
                          whenToDoNextAnimation: 0.8,
                          children: mails.map(
                            (mail) {
                              Widget _w = TinNhanBubble(
                                mail: mail,
                                key: Key(
                                  mail.mailId.toString(),
                                ),
                              );
                              return _w;
                            },
                          ).toList()
                            ..add(Padding(
                              padding: EdgeInsets.only(bottom: 10),
                            )),
                        ),
                      )
                    : Container(),
              ),
              Stack(children: [
                Column(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              controller: _textEditingController,
                              onChanged: (value) {
                                setState(() {
                                  content = value;
                                });
                              },
                              onTap: () {
                                _scrollController.animateTo(0,
                                    duration: Duration(milliseconds: 100),
                                    curve: Curves.easeOut);
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusColor: Colors.transparent,
                                  contentPadding: EdgeInsets.all(8),
                                  hintText: "Trao đổi tiếp"),
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.add_a_photo,
                              color: primaryColor,
                            ),
                            onPressed: getImageFromCamera,
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.add_photo_alternate,
                              color: primaryColor,
                            ),
                            onPressed: getImageFromFile,
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.send,
                              color: primaryColor,
                            ),
                            onPressed: guiTinNhan,
                          ),
                        ],
                      ),
                    ),
                    if (files.length > 0)
                      Container(
                        height: 20 * vh,
                        color: Colors.white,
                        child: ListView.builder(
                          itemCount: files.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return Stack(
                                alignment: Alignment.topRight,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(
                                        top: 8,
                                        bottom: 8,
                                        right: 8,
                                        left: index == 0 ? 8 : 0),
                                    child: Container(
                                      height: 140,
                                      width: 100,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          image: DecorationImage(
                                            image: FileImage(files[index]),
                                            fit: BoxFit.cover,
                                          )),
                                      child: Material(
                                        color: Colors.transparent,
                                        child: InkWell(
                                          splashColor:
                                              primaryColor.withOpacity(0.3),
                                          onTap: () {},
                                        ),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                      icon: Icon(
                                        Icons.remove_circle_outline,
                                        color: Colors.white,
                                      ),
                                      onPressed: () {
                                        files.removeAt(index);
                                        setState(() {
                                          files = files;
                                        });
                                      })
                                ]);
                          },
                        ),
                      )
                  ],
                ),
                if (sending)
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                    child: Container(
                        color: Color.fromRGBO(255, 255, 255, 0.8),
                        child: Center(child: CircularProgressIndicator())),
                  )
              ]),
            ],
          ),
        ),
      ],
    ));
  }
}
