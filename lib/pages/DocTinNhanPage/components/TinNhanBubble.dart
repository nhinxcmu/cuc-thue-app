import 'package:cuc_thue/consts.dart';
import 'package:cuc_thue/models/Mail.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/components/ListHinhAnh.dart';
import 'package:cuc_thue/pages/DanhSachTinNhanPage/components/TinNhanCard.dart';
import 'package:cuc_thue/styles/styles.dart';
import 'package:flutter/material.dart';

class TinNhanBubble extends StatefulWidget {
  Key key;
  Mail mail;
  TinNhanBubble({@required this.mail, this.key}) : super(key: key);
  @override
  _TinNhanBubbleState createState() => _TinNhanBubbleState();
}

class _TinNhanBubbleState extends State<TinNhanBubble>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double vw = MediaQuery.of(context).size.width / 100;
    return Padding(
      padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: widget.mail.sender.groupName == 'USER'
              ? MainAxisAlignment.end
              : MainAxisAlignment.start,
          children: <Widget>[
            widget.mail.sender.groupName != 'USER'
                ? CircleAvatar(
                    backgroundImage: NetworkImage(
                      host + '/dist/img/default-img.png',
                    ),
                  )
                : Container(),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Container(
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  color: widget.mail.sender.groupName != 'USER'
                      ? Colors.white
                      : Color.fromRGBO(224, 92, 63, 1),
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: boxShadow,
                ),
                constraints: BoxConstraints(maxWidth: 55 * vw),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.mail.sender.phone != null
                          ? widget.mail.sender.phone
                          : widget.mail.sender.fullName,
                      style: TextStyle(
                          color: widget.mail.sender.groupName == 'USER'
                              ? Colors.white.withOpacity(0.7)
                              : primaryColor.withOpacity(0.8)),
                    ),
                    Text(
                      widget.mail.content,
                      style: TextStyle(
                          color: widget.mail.sender.groupName == 'USER'
                              ? Colors.white
                              : Colors.black),
                    ),
                    if (widget.mail.attachments.length > 0)
                      ListHinhAnh(
                        urls: widget.mail.attachments
                            .map((attachment) => host + "/" + attachment.url)
                            .toList(),
                      )
                  ],
                ),
              ),
            ),
            widget.mail.sender.groupName == 'USER'
                ? CircleAvatar(backgroundImage: AssetImage('assets/avatar.png'))
                : Container(),
          ],
        ),
      ),
    );
  }
}
