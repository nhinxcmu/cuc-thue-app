import 'package:flutter/material.dart';

Color primaryColor = Color.fromRGBO(222, 81, 49, 1);
Color secondaryColor = Color.fromRGBO(52, 17, 9, 1);
List<BoxShadow> boxShadow = [
  BoxShadow(
      blurRadius: 5,
      color: Color.fromRGBO(0, 0, 0, 0.5),
      offset: Offset(1, 1),
      spreadRadius: 1)
];

class FadeInUpAnimation extends StatelessWidget {
  final Widget child;
  final Function onEnd;
  final Function callBack;
  FadeInUpAnimation({Key key, this.child, this.onEnd, this.callBack})
      : super(key: key);
  bool didCallBack = false;
  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder(
      onEnd: onEnd,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeOut,
      tween: Tween<double>(begin: 0, end: 1),
      builder: (context, double n, widget) {
        // if (n > timeToCallback && callBack != null && !didCallBack) {
        //   callBack();
        //   didCallBack = true;
        // }
        double paddingTop = 50 - (50 * n);
        return Padding(
          padding: EdgeInsets.only(top: paddingTop),
          child: Opacity(
            opacity: n,
            child: child,
          ),
        );
      },
    );
  }
}


