class Attachment {
  int attachId;
  String url;
  Attachment.fromMap(Map map) {
    this.attachId = map["attachId"];
    this.url = map["url"];
  }
}
