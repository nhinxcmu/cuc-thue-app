class Article {
  Article({this.articleId, this.title, this.image, this.description});
  int articleId;
  String title;
  DateTime date;
  String image;
  String description;
  Article.fromMap(Map map)
      : this.articleId = map['articleId'],
        this.title = map['title'].length <= 200
            ? map['title']
            : map['title'].substring(0, 200),
        this.date = DateTime.parse(map['date']),
        this.image = map['image'] == 'undefined'
            ? '/dist/img/default-img.png'
            : map['image'],
        this.description = map['description'].length <= 200
            ? map['description']
            : map['description'].substring(0, 200) + '...';
}
