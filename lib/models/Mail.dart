import 'package:cuc_thue/models/Attachment.dart';
import 'package:cuc_thue/models/User.dart';

class Mail {
  Mail({this.mailId, this.subject});
  int mailId;
  String subject;
  String content;
  DateTime sendDate;
  bool seen;
  bool close;
  DateTime closeDate;
  int parentMail;
  List<Attachment> attachments = [];
  double lat;
  double lon;
  int childCount;
  User sender;
  Mail.fromMap(Map map) {
    this.mailId = map['mailId'];
    this.subject = map['subject'];
    this.content = map['content'];
    this.sendDate = DateTime.parse(map['sendDate']);
    this.seen = map['seen'];
    this.close = map['close'];
    this.lat = map['lat'];
    this.lon = map['lon'];
    this.childCount = map['childCount'];
    map['attachments'].forEach((map) {
      attachments.add(Attachment.fromMap(map));
    });
    this.sender = User.fromMap(map['sender']);
  }
}
