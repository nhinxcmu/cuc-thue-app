class User {
  int userId;
  String userName;
  String fullName;
  String phone;
  String email;
  bool locked;
  bool female;
  DateTime birthDay;
  String groupName;
  User.fromMap(Map map){
    userId=map['userId'];
    userName=map['userName'];
    fullName=map['fullName'];
    phone=map['phone'];
    email=map['email'];
    locked=map['locked'];
    female=map['female'];
    birthDay=map['birthDay']!=null?DateTime.parse(map['birthDay']):null;
    groupName=map['groupName'];
  }
}